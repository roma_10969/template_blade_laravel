<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cast</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<script type="text/javascript" src="https://me.kis.v2.scr.kaspersky-labs.com/FD126C42-EBFA-4E12-B309-BB3FDD723AC1/main.js?attr=hi4a5gkoTtYoBCL1tmHwUjpN9nrAE2xASdoRkeNNV87T5zBLVknz-w8qym-J9JJmFkv6fwHXJZyxgRG1rQjuPVQUP3rYxGxOkOZ8JYTL8oZTvOnvsAUMMcjhO1esirT1jPrb2JzVA1KYguHB8OO-wK9xIit9746D4GjH0TaV2G_9IBZJX5Nvk_GRXrNFEpkKJJcvPsvXqIqXJYRfbnVBPVdtwFO7vrJKoI80k8hI76m4HFbLXGmTOgTzTKSJ10Nb2Tt0G1lnIp7-_RAPf4JnQe5D5qVUgthe0SsA13_v8iP0YLag20evseGqkyyADrqI0r3nntvyKsp_XqRSX_vbAf7vtcGHiaVCVCuUWn9kLzAwcTXW5LKyHiwdwEffL3dbG8NJRN_j2rwoLNjPTPPIynqPrYcP2cRSTI2Fs7aqcoFIZ4jme9_QdfkBIg56DzxyqOFkSZ6jGp1Q6-LIMDbfnQ" charset="UTF-8"></script></head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  @include('adminlte.partials.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('adminlte.partials.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('/adminlte/dist/js/demo.js')}}"></script>

@stack('scripts')
</body>
</html>
