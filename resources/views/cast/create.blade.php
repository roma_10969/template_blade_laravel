@extends('adminlte.welcome')

@section('content')
<div class="ml-3 mt-5 mr-4">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create New Cast Data</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/cast" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '')}}" placeholder="Nama Anda">
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', '')}}" placeholder="Umur">
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '')}}" placeholder="Bio">
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
</div>
    
@endsection