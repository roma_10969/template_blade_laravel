@extends('adminlte.welcome')

@section('content')
    <div class="ml-3 mt-5 mr-4">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Cast</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>Nama</th>
                        <th style="width: 30px">Umur</th>
                        <th>Bio</th>
                        <th style="width: 5px">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($cast as $key => $data)
                        <tr>
                            <td> {{$key + 1}} </td>
                            <td> {{$data->nama}} </td>
                            <td> {{$data->umur}} </td>
                            <td> {{$data->bio}} </td>
                            <td style="display: flex">
                                <a href="/cast/{{$data->id}}" class="btn btn-info btn-sm">Show</a>
                                <a href="/cast/{{$data->id}}/edit" class="btn btn-default btn-sm ml-2">Edit</a>
                                <form action="/cast/{{$data->id}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="5" align="center">No Cast</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <a class="btn btn-outline-secondary mt-2 " href="/cast/create">Create New Cast</a>
            </div>
            <!-- /.card-body -->
         <!--
            <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
              </ul>
            </div>
        -->
          </div>
    </div>

@endsection