@extends('adminlte.welcome')

@section('content')   
    <div class="ml-3 mt-5 mr-4">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Cast Description</h3>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-lg-2"><h6>Nama</h6></div>
                    <div class="col-sm-"><h6>:</h6></div>
                    <div class="col"><h6>{{$cast->nama}}</h6></div>
                </div>
                <div class="row">
                    <div class="col-lg-2"><h6>Umur</h6></div>
                    <div class="col-sm-"><h6>:</h6></div>
                    <div class="col"><h6>{{$cast->umur}}</h6></div>
                </div>
                <div class="row">
                    <div class="col-lg-2"><h6>Bio</h6></div>
                    <div class="col-sm-"><h6>:</h6></div>
                    <div class="col"><h6>{{$cast->bio}}</h6></div>
                </div>
            </div>

        </div>
    </div>
@endsection